﻿using System;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace MekCombat {
    public partial class Game1 : Game {
        private GraphicsDeviceManager graphics;
        private int screen_width = 832;
        private int screen_height = 400;
        private SpriteBatch spriteBatch;
        private SpriteFont font;
        //
        //
        public MouseState mouseState;
        //
        // In-Game Assets
        private Texture2D ground_tiles;
        //
        int game_state;
        bool mouse_is_down = false;
        bool mouse_is_up = true;
        //
        Random rng = new Random();
        int rng_value;
        //
        private Debug.ConsoleDebugLogger _Log;
        //
        int a;
        int b;
        int c;
        //
        public Game1() {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            //
            //
            game_state = 0;
            // Game States:
            // 0 - Main Menu (select host or connect to server)
            // 1 - Host Game
            // 2 - Join Game
            // 3 - In-Game
            // 4 - End-Game (declate winner, restart match, etc)
        }

        protected override void Initialize() {
            // TODO: Add your initialization logic here
            graphics.PreferredBackBufferWidth = screen_width;
            graphics.PreferredBackBufferHeight = screen_height;
            //
            _Log = Debug.ConsoleDebugLogger.GetDebugger();
            //
            graphics.ApplyChanges();
            //
            font = Content.Load<SpriteFont>("spritefontfile");
            ground_tiles = Content.Load<Texture2D>("hex_tiles");
            //
            reset_game();
            //
            base.Initialize();
        }

        protected override void LoadContent() {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        protected override void Update(GameTime gameTime) {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape)) {
                Exit();
            }
            //
            //
            //
            mouse_is_down = false;
            mouseState = Mouse.GetState();
            if (mouseState.LeftButton == ButtonState.Pressed) {
                if (mouse_is_down == false && mouse_is_up == true) {
                    mouse_is_down = true;
                    mouse_is_up = false;
                }
            }
            if (mouseState.LeftButton == ButtonState.Released) {
                mouse_is_up = true;
            }
            //
            //
            //
            switch (game_state) {
                case 0: // Main Menu
                    if (mouse_is_down == true) {
                        if (mouseState.Y > 40 && mouseState.Y < 55) {
                            game_state = 1;
                        } else if (mouseState.Y > 55 && mouseState.Y < 70) {
                            game_state = 2;
                        }
                    }
                    break;
                case 1: // Host Game
                    if (mouse_is_down == true) {
                        game_state = 3;
                    }
                    break;
                case 2: // Join Game
                    if (mouse_is_down == true) {
                        game_state = 3;
                    }
                    break;
                case 3: // In-Game
                    //
                    do_player_turn(player_turn);
                    //
                    break;
                case 4: // End-Game / Restart Match
                    if (mouse_is_down == true) {
                        reset_game();
                        game_state = 0;
                    }
                    break;
            }
            //
            //
            //
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime) {
            //
            // Render game graphics
            //
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp); // Point Clamp for pixel art (round pixels to nearest neighbor when scaling)
            //
            switch (game_state) {
                case 0: // Main Menu
                    spriteBatch.DrawString(font, "Game Mode: Main Menu", new Vector2(10, 10), Color.White);
                    spriteBatch.DrawString(font, "Host Game", new Vector2(20, 40), Color.White);
                    spriteBatch.DrawString(font, "Join Game", new Vector2(20, 55), Color.White);
                    GraphicsDevice.Clear(new Color(0x111199));
                    break;
                case 1: // Host Game
                    spriteBatch.DrawString(font, "Game Mode: Host Game", new Vector2(10, 10), Color.White);
                    GraphicsDevice.Clear(new Color(0x1111FF));
                    break;
                case 2: // Find Game
                    spriteBatch.DrawString(font, "Game Mode: Join Game", new Vector2(10, 10), Color.White);
                    GraphicsDevice.Clear(new Color(0x1166FF));
                    break;
                case 3: // In-Game
                    //
                    // Draw tile map in the jankiest way possible.  This will be done away with.
                    //
                    for (b = -4; b < 36; b++) {
                        for (a = -3; a < 14; a++) {
                            if (a < 1 || a > 11 || b < 3 || b > 19) {
                                if (b % 2 == 0) {
                                    spriteBatch.Draw(ground_tiles, new Rectangle(a * 64, 48 + b * 16, 48, 48), new Rectangle(24, 24, 24, 24), Color.White);
                                } else {
                                    spriteBatch.Draw(ground_tiles, new Rectangle(34 + a * 64, 48 + b * 16, 48, 48), new Rectangle(24, 24, 24, 24), Color.White);
                                }
                            } else {
                                if (b % 2 == 0) {
                                    spriteBatch.Draw(ground_tiles, new Rectangle(a * 64, 48 + b * 16, 48, 48), new Rectangle(0, 0, 24, 24), Color.White);
                                } else {
                                    spriteBatch.Draw(ground_tiles, new Rectangle(34 + a * 64, 48 + b * 16, 48, 48), new Rectangle(24, 0, 24, 24), Color.White);
                                }
                            }
                        }
                    }
                    //
                    //
                    //
                    spriteBatch.DrawString(font, "Game Mode: In-Game", new Vector2(10, 10), Color.White);
                    switch (game_phase) {
                        case 0:
                        spriteBatch.DrawString(font, "Click to roll initiative", new Vector2(30, 50), Color.White);
                            spriteBatch.DrawString(font, "Current Turn: " + player_units[player_turn].mech_name, new Vector2(30, 70), Color.White);
                            break;
                        case 1:
                            spriteBatch.DrawString(font, "Move Phase", new Vector2(30, 50), Color.White);
                            break;
                        case 2:
                            spriteBatch.DrawString(font, "Attack Phase", new Vector2(30, 50), Color.White);
                            spriteBatch.DrawString(font, "Current Turn: " + player_units[player_order[player_turn]].mech_name, new Vector2(30, 70), Color.White);
                            //
                            //
                            for (a = 0; a < number_of_players_connected; a++) {
                                //
                                int x = player_units[player_order[a]].global_x;
                                int y = player_units[player_order[a]].global_y;
                                //
                                if (player_turn == a) {
                                    spriteBatch.Draw(ground_tiles, new Rectangle(x, y, 104, 178), new Rectangle(104, 48, 104, 178), Color.White);
                                    //
                                    if (selected_player > -1) {
                                        render_text(x + 4, y + 165, "Attack: " + player_units[selected_player].mech_name);
                                    } else {
                                        render_text(x + 4, y + 165, "Select target");
                                    }
                                    //spriteBatch.DrawString(font, "Defend", new Vector2(x + 50, y + 160), Color.Black);
                                } else {
                                    if (selected_player == player_order[a]) {
                                        spriteBatch.Draw(ground_tiles, new Rectangle(x, y, 104, 178), new Rectangle(208, 48, 104, 178), Color.White);
                                    } else {
                                        spriteBatch.Draw(ground_tiles, new Rectangle(x, y, 104, 178), new Rectangle(0, 48, 104, 178), Color.White);
                                    } 
                                }
                                //
                                //
                                render_text(x + 4, y + 140, player_units[player_order[a]].mech_name);
                                render_text(x + 4, y + 150, "HP: " + player_units[player_order[a]].hp);
                                //
                                if (player_units[player_order[a]].hp < 1) {
                                    spriteBatch.Draw(ground_tiles, new Rectangle(x + 16, y + 16, 72, 72), new Rectangle(312, 48, 72, 72), Color.White);
                                }
                            }
                            //
                            //
                            break;
                        }
                    //
                    GraphicsDevice.Clear(new Color(0x991111));
                    break;
                case 4: // End-Game / Restart Match
                    spriteBatch.DrawString(font, "Game Mode: End Game", new Vector2(10, 10), Color.White);
                    GraphicsDevice.Clear(new Color(0x111111));
                    break;
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
        //
        //
        public void render_text(int x, int y, string text) {
            //
            int ta;
            int tb;
            //
            byte[] ascii_bytes;
            ascii_bytes = Encoding.ASCII.GetBytes(text);
            //
            for (tb = 0; tb < ascii_bytes.Length; tb++) {
                if (ascii_bytes[tb] != 32) {
                    if (ascii_bytes[tb] > 96 && ascii_bytes[tb] < 123) {
                        ascii_bytes[tb] -= 32;
                    }
                    switch (ascii_bytes[tb]) {
                        case 60: // LEFT ARROW
                            ta = 8 * 14;
                            spriteBatch.Draw(ground_tiles, new Rectangle(x + (7 * tb), y, 8, 8), new Rectangle(ta, 0, 8, 8), Color.White);
                            break;
                        case 62: // RIGHT ARROW
                            ta = 8 * 15;
                            spriteBatch.Draw(ground_tiles, new Rectangle(x + (7 * tb), y, 8, 8), new Rectangle(ta, 0, 8, 8), Color.White);
                            break;
                        case 94: // UP ARROW
                            ta = 8 * 12;
                            spriteBatch.Draw(ground_tiles, new Rectangle(x + (7 * tb), y, 8, 8), new Rectangle(ta, 0, 8, 8), Color.White);
                            break;
                        case 118: // DOWN ARROW
                            ta = 8 * 13;
                            spriteBatch.Draw(ground_tiles, new Rectangle(x + (7 * tb), y, 8, 8), new Rectangle(ta, 0, 8, 8), Color.White);
                            break;
                        case 46: // Period / Decimal
                            ta = 376;
                            spriteBatch.Draw(ground_tiles, new Rectangle(x + (7 * tb), y, 8, 8), new Rectangle(ta, 0, 8, 8), Color.White);
                            break;
                        case 45: // - Sign
                            ta = 384;
                            spriteBatch.Draw(ground_tiles, new Rectangle(x + (7 * tb), y, 8, 8), new Rectangle(ta, 0, 8, 8), Color.White);
                            break;
                        default:
                            ta = 192 + 8 * (ascii_bytes[tb] - 65);
                            spriteBatch.Draw(ground_tiles, new Rectangle(x + (7 * tb), y, 6, 8), new Rectangle(ta, 0, 6, 8), Color.White);
                            break;
                    }
                }
            }
        }
        //
        //
    }
}
