﻿using System.Collections.Generic;

namespace MekCombat {
    public partial class Game1 {
        //
        const int max_number_of_players = 30;
        int number_of_players_connected = 6; //
        //
        static List<Unit> player_units = new List<Unit>();
        static int[] player_initiative = new int[max_number_of_players];
        static int[] player_order = new int[max_number_of_players];
        //
        static string[] player_names = new string[] { "Joseph", "Jack", "Sean", "Jordan", "Mike", "Dale", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" };
        //
        public float[] map;

        //
        public int player_turn = 0;
        public int game_phase = 0;
        public int selected_player;
        //
        public void reset_game() {
            //
            //
            player_units.Clear();
            for (a = 0; a < max_number_of_players; a++) {
                player_initiative[a] = 0;
                player_order[a] = 0;
            }
            //
            for (a = 0; a < max_number_of_players; a++) {
                player_units.Add(new Unit());
                player_units[a].player_owner = a;
                player_units[a].hp = 20;
                player_units[a].mech_name = player_names[a];
                player_units[a].global_x = 60 + a * 120;
                player_units[a].global_y = 140;
            }
            player_turn = 0;
            game_phase = 0;
            _Log.Write("Game Reset");
            //
            //
        }
        //
        //
        //
        public void do_player_turn(int player) {
            switch (game_phase) {
                case 0:
                    //
                    // Initiative phase - All players roll for initiative.
                    // If the top numbers are tied, reroll until tiebreak.
                    // Simulate D100 for less likely ties.
                    //
                    bool all_players_have_rolled = true;
                    for (a = 0; a < number_of_players_connected; a++) {
                        if (player_initiative[a] == 0) {
                            all_players_have_rolled = false;
                        }
                    }
                    if (all_players_have_rolled == false) {
                        mouse_is_down = true; // Auto roll initiative - Remove this later
                        if (mouse_is_down == true) {
                            rng_value = rng.Next(100) + 1;
                            player_initiative[player] = rng_value;
                            _Log.Write("Player " + player_units[player].mech_name + " rolled a " + rng_value + " for initiative.");
                            //
                            player_turn++;
                            //
                            for (a = 0; a < number_of_players_connected; a++) {
                                if (player_initiative[a] == 0) {
                                    all_players_have_rolled = false;
                                }
                            }
                        }
                    } else {
                        //
                        // Sort players into initiative order
                        // TODO: Tiebreaker- currently sorts giving priority to whoever's earlier in the list
                        //
                        _Log.Write("All players have rolled.  The player order will be: ");
                        int highest_roll = 0;
                        int player_with_highest_roll = 0;
                        for (b = 0; b < number_of_players_connected; b++) {
                            highest_roll = 0;
                            player_with_highest_roll = 0;
                            for (a = 0; a < number_of_players_connected; a++) {
                                if (player_initiative[a] > highest_roll) {
                                    highest_roll = player_initiative[a];
                                    player_with_highest_roll = a;
                                }
                            }
                            player_order[b] = player_with_highest_roll;
                            player_initiative[player_with_highest_roll] = 0;
                            _Log.Write("~~ Player " + player_units[player_with_highest_roll].mech_name + " ");
                        }
                        //
                        // Double check that the order array is correct
                        // And include the original indexes of the players
                        //
                        for (a = 0; a < number_of_players_connected; a++) {
                            _Log.Write("Player Order Array: " + player_units[player_order[a]].mech_name + " - Index: " + player_order[a]);
                        }
                        //
                        // Proceed to move phase, reset player turn
                        //
                        game_phase = 1;
                        player_turn = 0;
                        selected_player = -1;
                        _Log.Write("");
                    }
                    break;
                case 1:
                    //
                    // Move phase
                    // TODO: Move phase lol
                    //
                    _Log.Write("All players have moved into position.");
                    _Log.Write("");
                    player_turn = 0;
                    selected_player = -1;
                    game_phase = 2;
                    break;
                case 2:
                    //
                    // Attack phase
                    //
                    if (player_units[player_order[player]].hp > 0) { // The player must be alive to attack.
                        if (mouse_is_down == true) {
                            //
                            //
                            //
                            if (mouseState.X > player_units[player_order[player_turn]].global_x && mouseState.X < player_units[player_order[player_turn]].global_x + 104) {
                                if (mouseState.Y > player_units[player_order[player_turn]].global_y && mouseState.Y < player_units[player_order[player_turn]].global_y + 160) {
                                    if (selected_player > -1) { // A target has been selected
                                        _Log.Write("Player " + player_units[player_order[player]].mech_name + " selected Player " + player_units[selected_player].mech_name + " to attack.");
                                        rng_value = rng.Next(6) + 1;
                                        if (player_units[selected_player].hp > 0) {
                                            player_units[selected_player].hp -= rng_value;
                                            if (player_units[selected_player].hp < 0) {
                                                player_units[selected_player].hp = 0;
                                            }
                                            _Log.Write("Player " + player_units[selected_player].mech_name + "'s mech took " + rng_value + " damage.  Player " + player_units[selected_player].mech_name + "'s mech has " + player_units[selected_player].hp + " HP remaining. \r\n");
                                        } else {
                                            _Log.Write("Player " + player_units[player_order[player]].mech_name + " is beating the already dead corpse of Player " + player_units[selected_player].mech_name + "'s mech.  Stop that. \r\n");
                                        }
                                        selected_player = -1;
                                        //
                                        // Display list of all players' HP
                                        //
                                        for (a = 0; a < number_of_players_connected; a++) {
                                            _Log.Write("Player " + player_units[a].mech_name + " HP: " + player_units[a].hp);
                                            //player_units[a].global_x = rng.Next(500) + 50;
                                            player_units[a].global_y = rng.Next(200) + 50;
                                        }
                                        _Log.Write("");
                                        player_turn++;
                                        if (player_turn > number_of_players_connected - 1) {
                                            player_turn = 0;
                                        }
                                        //
                                        // Check win-state (all mechs at 0 health except winner)
                                        //
                                        int num_of_living_players = number_of_players_connected;
                                        for (a = 0; a < number_of_players_connected; a++) {
                                            if (player_units[a].hp < 1) {
                                                num_of_living_players--;
                                            }
                                        }
                                        if (num_of_living_players < 2) {
                                            int surviving_player = -1;
                                            for (a = 0; a < number_of_players_connected; a++) {
                                                if (player_units[a].hp > 0) {
                                                    surviving_player = a;
                                                }
                                            }
                                            //
                                            // Move game state to end screen
                                            //
                                            game_state = 4;
                                            _Log.Write("Player " + player_units[surviving_player].mech_name + " is victorious! \r\n");
                                        }
                                    }
                                }
                            } else { // A target has not been selected
                                for (a = 0; a < number_of_players_connected; a++) {
                                    //
                                    if (a != player_turn) {
                                        if (mouseState.X > player_units[player_order[a]].global_x && mouseState.X < player_units[player_order[a]].global_x + 104) {
                                            if (mouseState.Y > player_units[player_order[a]].global_y && mouseState.Y < player_units[player_order[a]].global_y + 160) {
                                                //
                                                //
                                                selected_player = player_order[a];
                                                _Log.Write("Selected Player: " + a);
                                                a = 9999;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //
                        //
                    } else {
                        //
                        // When player is knocked out skip their turn
                        //
                        _Log.Write("Player " + player_units[player_order[player]].mech_name + "'s mech is destroyed and cannot attack. \r\n");
                        player_turn++;
                        if (player_turn > number_of_players_connected - 1) {
                            player_turn = 0;
                        }
                    }
                    break;
            }
        }
        //
        //
        //
        class Unit {
            //
            //
            public string mech_name;
            public int player_owner;
            public int mech_class;
            public int mech_type;
            public int hp = 100;
            public int weapon_01;
            public int weapon_02;
            public int weapon_03;
            public int weapon_04;
            public int grid_x;
            public int grid_y;
            public int grid_z;
            public int local_x;
            public int local_y;
            public int local_z;
            public int global_x;
            public int global_y;
            public int global_z;
            //
            //
        }
        //
    }
}
