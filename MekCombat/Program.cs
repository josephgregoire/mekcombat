﻿using System;

namespace MekCombat {
    public static class Program {
        [STAThread]
        static void Main(string [] args)
        {
            /*
            #region Some Testing stuff
            Network.GameServer server = null;
            Network.GameClient client = null;

            var Log = Debug.ConsoleDebugLogger.GetDebugger();
            Log.EnableDebugging();

            Log.Write("Program has started");

            foreach (string arg in args)
            {
                if (arg.Equals("server"))
                {
                    server = Network.GameServer.GetServer();
                    server.StartServer();
                }
            }
            #endregion
            */

            using (var game = new Game1())
                game.Run();
        }
    }
}
