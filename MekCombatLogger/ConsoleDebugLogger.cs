﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace MekCombat.Debug
{
    public class ConsoleDebugLogger
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool AllocConsole();
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();
        [DllImport("kernel32.dll")]
        static extern bool FreeConsole();
        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        private const int SW_HIDE = 0;
        private const int SW_SHOW = 5;

        private static ConsoleDebugLogger _INSTANCE = null;
        private IntPtr _consoleHandle;
        private string _fileName = string.Empty;

        public bool Enabled { get; private set; } = false;
        public bool FileEnabled { get; private set; } = false;

        public static ConsoleDebugLogger GetDebugger()
        {
            if (_INSTANCE == null)
                _INSTANCE = new ConsoleDebugLogger();

            return _INSTANCE;
        }

        private ConsoleDebugLogger()
        {
            _consoleHandle = IntPtr.Zero;
        }

        public void EnableDebugging()
        {
            if (!Enabled)
            {
                AllocConsole();
                _consoleHandle = GetConsoleWindow();

                if (_consoleHandle != IntPtr.Zero)
                {
                    ShowWindow(_consoleHandle, SW_SHOW);
                    Enabled = true;
                }
            }
        }

        public void DisableDebugging()
        {
            if ((_consoleHandle != IntPtr.Zero) && Enabled)
            {
                ShowWindow(_consoleHandle, SW_HIDE);
                FreeConsole();
                _consoleHandle = IntPtr.Zero;
                Enabled = false;
            }
        }

        public void EnablingFileLogging(string fileName)
        {
            FileEnabled = true;
            _fileName = fileName;
        }

        public void Write(string format, params object[] args)
        {
            if (Enabled)
                Console.WriteLine(((args == null) || args.Length < 1) ? format : format, args);

            if (FileEnabled)
                File.AppendAllText(_fileName, ((args == null) || args.Length < 1) ? format + "\r\n" : string.Format(format, args) + "\r\n");

        }
    }
}
