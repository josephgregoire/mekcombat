﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace MekCombat.Network
{
    public class GameClient
    {
        private Debug.ConsoleDebugLogger Log;
        private IPEndPoint _endpoint;
        private GameSocket _serverSocket;
        private ConcurrentQueue<Message> _messageQueue;
        private Thread _messageProcessingThread;
        private bool _connected;

        public int ClientID { get; private set; }

        public ServerMessageDelegate ServerMessageHandler;
        public StatusDelegate DisconnectedHandler;

        public GameClient()
        {
            Log = Debug.ConsoleDebugLogger.GetDebugger();
            _connected = false;
        }

        public GameClient(string ipAddress, int port) : this()
        {
            SetServerAddress(ipAddress, port);
        }

        public bool SetServerAddress(string ipAddress, int port)
        {
            try
            {
                IPAddress addr = IPAddress.Parse(ipAddress);
                _endpoint = new IPEndPoint(addr, port);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool StartClientConnection()
        {
            try
            {
                Log.Write("StartClientConnection() called");

                _serverSocket = new GameSocket();
                _serverSocket.Connect(_endpoint);

                if (_serverSocket.IsConnected)
                {
                    Log.Write("GameClient is connected to server {0}:{1}", _endpoint.Address.ToString(), _endpoint.Port);

                    _serverSocket.MessageReceived += OnMessageRecieved;
                    _serverSocket.Disconnected += OnServerDisconnected;
                    _serverSocket.ConnectionError += OnConnectionError;
                    _serverSocket.StartReceiving();
                    _connected = true;

                    _messageQueue = new ConcurrentQueue<Message>();
                    _messageProcessingThread = new Thread(new ThreadStart(MainMessageProcessingLoop)) { IsBackground = true };
                    _messageProcessingThread.Start();
                }
            }
            catch (Exception e)
            {
                Log.Write("StartClientConnection() Error: {0}", e.ToString());
                return false;
            }

            return true;
        }

        private void MainMessageProcessingLoop()
        {
            while (_connected)
            {
                Message message;
                if (!_messageQueue.TryDequeue(out message))
                {
                    Thread.Sleep(10);
                    continue;
                }

                // GameServer handles creating and assigning the unique 4-digit ClientIds, and this message is to update the GameClient with the matching ID
                if (message.Type == MessageType.UPDATE_CLIENT_ID)
                {
                    UpdateClientId(message.SourceID);
                    continue;
                }
                
                ProcessMessage(message);
            }
        }

        private void UpdateClientId(int id)
        {
            this._serverSocket.ID = id;
            this.ClientID = id;
        }

        public bool IsConnected()
        {
            return _serverSocket.IsConnected;
        }

        private void ProcessMessage(Message msg)
        {
            ServerMessageHandler?.Invoke(msg);
        }

        public void SendRequest(Message msg)
        {
            _serverSocket.AddMessage(msg);
            Log.Write(Utility.MessageLog("Message Sent", _serverSocket.EndPoint.ToString(), msg));
        }

        public void Disconnect()
        {
            _serverSocket.Disconnect();
            Log.Write("Called Disconnect()");
        }

        #region Event Callbacks
        private void OnMessageRecieved(int id, Message msg)
        {
            if (Log.Enabled)
                Log.Write(Utility.MessageLog("Message Recieved", _serverSocket.EndPoint.ToString(), msg));

            _messageQueue.Enqueue(msg);
        }

        private void OnServerDisconnected(int serverId)
        {
            Log.Write("WARNING! Disconnected from server");

            _connected = false;
            DisconnectedHandler?.Invoke(serverId);
        }

        private void OnConnectionError(int clientId, Exception error)
        {
            Log.Write("ERROR! Socket exception was thrown: {0}", error);
        }
        #endregion
    }
}
