﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace MekCombat.Network
{
    /// <summary>
    /// This class has a more compact serialization method than the standard serialization methods in C#'s stdlib.
    /// Instead of creating a complex (and bloated) object graph thats 1k bytes in size + the data, the functionality of this static class is to instead provide
    /// a more compact form of serialization specifically for the Message class for TCP communications, while still providing modularity in the metadata.
    /// 
    /// Essentially, the Message object has 2 parts: The header information, and the data. The header information will be
    /// 12 bytes in length, always. As for the data, it is written to a byte format utilizing the following pattern for its objects...
    ///    offset 0: type (i.e. "int" or "float")
    ///    offset 1: value
    ///    
    /// This will work for all primitive types except for strings, which follow a different pattern...
    ///    offset 0: type (in this case "string")
    ///    offset 1: length of the string, in ushort
    ///    offset 3: string value
    ///    
    /// This way, we can keep a more compacted size of the message object, without secrificing the modular metadata
    /// </summary>
    public static class MessageSerializer
    {
        public static byte[] Serialize(Message msg)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                using (BinaryWriter bw = new BinaryWriter(ms))
                {
                    // header info
                    bw.Write(msg.SourceID);
                    bw.Write((int)msg.Type);
                    bw.Write(msg.MessageCode);

                    // number of data elements
                    bw.Write((ushort)(msg.Data == null ? 0 : msg.Data.Length));

                    // data info
                    if ((msg.Data != null) && (msg.Data.Length > 0))
                    {
                        foreach (object data in msg.Data)
                        {
                            // switch statements cant use the typeof keyword in their case statements... so convoluted if/else tree instead
                            if (data.GetType() == typeof(int))
                            {
                                bw.Write((byte)1);
                                bw.Write((int)data);
                                continue;
                            }
                            else if (data.GetType() == typeof(long))
                            {
                                bw.Write((byte)2);
                                bw.Write((long)data);
                                continue;
                            }
                            else if (data.GetType() == typeof(float))
                            {
                                bw.Write((byte)3);
                                bw.Write((float)data);
                                continue;
                            }
                            else if (data.GetType() == typeof(bool))
                            {
                                bw.Write((byte)4);
                                bw.Write((bool)data);
                                continue;
                            }
                            else // make string to be the fallback
                            {
                                bw.Write((byte)9);
                                byte[] strbuf = Encoding.UTF8.GetBytes((string)data);
                                ushort len = (ushort)strbuf.Length;
                                bw.Write(len);
                                bw.Write(strbuf);
                                continue;
                            }
                        }
                    }

                    // Due to how the C# stream classes increment the stream pointer during reading, we get to a point where we hit the end
                    // of the stream and it throws an exception (EndOfStream), so just write an ending dummy byte of 0xFF
                    bw.Write((byte)0xff);
                }

                return ms.ToArray();
            }
        }

        public static Message Deserialize(byte[] msgData)
        {
            using (MemoryStream ms = new MemoryStream(msgData))
            {
                using (BinaryReader br = new BinaryReader(ms))
                {
                    Message msg = new Message();

                    // read header info
                    msg.SourceID = br.ReadInt32();
                    msg.Type = (MessageType)br.ReadInt32();
                    msg.MessageCode = br.ReadInt32();

                    // get the number of Data objects
                    ushort count = br.ReadUInt16();

                    // if we have at least 1+ data object, then continue reading the rest of the stream
                    if (count > 0)
                    {
                        msg.Data = new object[count];

                        try
                        {
                            for (int i = 0; i < count; i++)
                            {
                                byte type = br.ReadByte();

                                switch (type)
                                {
                                    case 1: msg.Data[i] = br.ReadInt32(); break;
                                    case 2: msg.Data[i] = br.ReadInt64(); break;
                                    case 3: msg.Data[i] = br.ReadSingle(); break;
                                    case 4: msg.Data[i] = br.ReadBoolean(); break;
                                    case 9:
                                        ushort strlen = br.ReadUInt16();
                                        byte[] str = br.ReadBytes(strlen);
                                        msg.Data[i] = Encoding.UTF8.GetString(str);
                                        break;
                                }
                            }
                        }
                        catch (EndOfStreamException e)
                        {
                            // If we happen to read the end of the stream where it throws this exception, we dont care. Just end gracefully
                            // UPDATE: I added a dummy byte of 0xFF to the end instead, so we should never hit this exception anymore
                        }
                    }

                    return msg;
                }
            }
        }
    }
}
