﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace MekCombat.Network
{
    public class GameServer
    {
        private static GameServer _INSTANCE;
        private Dictionary<int, GameSocket> _playerClients;
        private TcpListener _gameServerListener;
        private ConcurrentQueue<Message> _messageQueue;
        private Thread _messageProcessingThread;
        private bool _serverRunning;
        private AutoResetEvent _connectionWait = new AutoResetEvent(false);

        private const int SERVER_ID = 0;

        private Debug.ConsoleDebugLogger Log;

        public ClientMessageDelegate ClientMessageHandler;
        public StatusDelegate ClientConnectedHandler;
        public StatusDelegate ClientDisconnectedHandler;

        public int NumOfClients { get; private set; } = 0;

        public static GameServer INSTANCE
        {
            get
            {
                if (_INSTANCE == null)
                    _INSTANCE = new GameServer();

                return _INSTANCE;
            }
        }

        private GameServer()
        {
            Log = Debug.ConsoleDebugLogger.GetDebugger();
        }

        public void StartServer()
        {
            Log.Write("StartServer() called");

            _playerClients = new Dictionary<int, GameSocket>();
            NumOfClients = 0;

            _gameServerListener = new TcpListener(IPAddress.Any, Constants.PORT);
            _gameServerListener.Start();

            _serverRunning = true;

            // Start a separate thread to handle incoming connection requests
            Thread listenerThread = new Thread(new ThreadStart(() =>
            {
                Log.Write("Started server listening on port {0}", Constants.PORT);

                while (_serverRunning)
                {
                    _gameServerListener.BeginAcceptTcpClient(new AsyncCallback(OnClientConnected), null);
                    _connectionWait.WaitOne();
                    _connectionWait.Reset();
                }
            }))
            { IsBackground = true };
            listenerThread.Start();

            // Start the main message processing loop for the server
            _messageQueue = new ConcurrentQueue<Message>();
            _messageProcessingThread = new Thread(new ThreadStart(MainMessageProcessingLoop)) { IsBackground = true };
            _messageProcessingThread.Start();

            Log.Write("Server has now fully started!");
        }

        public void StopServer()
        {
            foreach (GameSocket pClient in _playerClients.Values)
                pClient.Disconnect();

            _serverRunning = false;
            _connectionWait.Set();
        }

        private void MainMessageProcessingLoop()
        {
            while (_serverRunning)
            {
                Message message;
                if (!_messageQueue.TryDequeue(out message))
                {
                    Thread.Sleep(10);
                    continue;
                }

                ProcessRequest(message);
            }
        }

        #region Callback Methods
        private void OnClientConnected(IAsyncResult ar)
        {
            TcpClient client = _gameServerListener.EndAcceptTcpClient(ar);

            if (++NumOfClients > Constants.MAX_PLAYERS)
                throw new ArgumentOutOfRangeException(); //will need to properly consume this error later, but for now, throw an exception

            int newId = CreateUniqueClientId();

            GameSocket pClient = new GameSocket()
            {
                Client = client,
                ID = newId,
            };
            pClient.MessageReceived += OnMessageRecieved;
            pClient.Disconnected += OnClientDisconnected;
            pClient.ConnectionError += OnConnectionError;
            pClient.StartReceiving();
            Log.Write("Player Client ID {0} has connected from {1}", pClient.ID, pClient.EndPoint.ToString());

            _playerClients.Add(pClient.ID, pClient);

            // Send message to remote client to update their endpoint's client Id so that they match
            Message msg = new Message()
            {
                Type = MessageType.UPDATE_CLIENT_ID,
                MessageCode = 0,
                SourceID = pClient.ID,
                Data = null
            };
            SendMessageToClient(pClient.ID, msg);

            ClientConnectedHandler?.Invoke(pClient.ID);
            _connectionWait.Set();
        }

        private void OnMessageRecieved(int id, Message message)
        {
            if (Log.Enabled) // Check to prevent un-necessary overhead retreiving and referencing GameSocket objects for logging
            {
                GameSocket pClient;
                if (_playerClients.TryGetValue(id, out pClient))
                    Log.Write(Utility.MessageLog("Message Recieved", pClient.EndPoint.ToString(), message));
            }

            _messageQueue.Enqueue(message);
        }

        private void OnClientDisconnected(int clientId)
        {
            GameSocket pClient;
            if (!_playerClients.TryGetValue(clientId, out pClient))
                return;

            Log.Write("Client ({0}) disconnected from the server", pClient.EndPoint.ToString());
            _playerClients.Remove(clientId);

            ClientDisconnectedHandler?.Invoke(clientId);
        }

        private void OnConnectionError(int clientId, Exception error)
        {
            GameSocket pClient;
            if (_playerClients.TryGetValue(clientId, out pClient))
                Log.Write("ERROR! Client ID {2} ({0}) socket threw exception: {1}", pClient.EndPoint.ToString(), error, pClient.ID);
            else
                Log.Write("ERROR! General socket exception thrown: {0}", error);
        }
        #endregion

        private void ProcessRequest(Message message)
        {
            ClientMessageHandler?.Invoke(message.SourceID, message);
        }

        public void SendMessageToClient(int id, Message message)
        {
            if (_playerClients.TryGetValue(id, out GameSocket pClient))
                pClient.AddMessage(message);

            Log.Write(Utility.MessageLog("Message Sent", pClient.EndPoint.ToString(), message));
        }

        public void SendMessageAll(Message message)
        {
            foreach (GameSocket pClient in _playerClients.Values)
                pClient.AddMessage(message);

            Log.Write(Utility.MessageLog("Message Sent", "ALL CLIENTS", message));
        }

        public GameSocket GetClient(int clientId)
        {
            if (_playerClients.TryGetValue(clientId, out GameSocket pClient))
                return pClient;
            return null;
        }

        private int CreateUniqueClientId()
        {
            Random rand = new Random();
            int createdId = rand.Next(1000, 9999);

            while (_playerClients.TryGetValue(createdId, out GameSocket dummy))
                createdId = rand.Next(1000, 9999);

            return createdId;
        }

        public void DisconnectClient(int cliendId)
        {
            GameSocket pClient;
            if (_playerClients.TryGetValue(cliendId, out pClient))
                pClient.Disconnect();
        }
    }
}
