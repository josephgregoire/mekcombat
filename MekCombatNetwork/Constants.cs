﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MekCombat.Network
{
    public delegate void ClientMessageDelegate(int ID, Message msg);
    public delegate void ServerMessageDelegate(Message msg);
    public delegate void StatusDelegate(int ID);
    public delegate void ErrorDelegate(int ID, Exception error);

    public enum MessageType
    {
        CLIENT_REQUEST = 0,
        SERVER_RESPONSE = 1,
        STATUS = 2,
        STATE_CHANGE = 3,
        UPDATE_CLIENT_ID = 9999  // This MessageType is ONLY used when a client connects and the server needs to update the Client of the Server's assigned Client Id.
    }

    public static class Constants
    {
        public const int MAX_PLAYERS = 30;
        public const int TICKS_PER_SEC = 30;
        public const float MS_PER_TICK = 1000 / TICKS_PER_SEC;

        public const int PORT = 4500;

        public const int MESSAGE_BUFFER_SIZE = 4096;
    }

    public class Utility
    {
        public static string MessageLog(string type, string ip, Message msg)
        {
            string text = string.Format("[{0}, {1}]   Data objects: {2}", Enum.GetName(typeof(MessageType), msg.Type), msg.MessageCode, msg.Data?.Length);
            return string.Format("{0} {1} {2}", type.PadRight(19), ip.PadRight(20), text);
        }
    }
}
