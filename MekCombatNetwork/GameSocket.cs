﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace MekCombat.Network
{
    public class GameSocket
    {
        public bool IsConnected { get; private set; }
        public TcpClient Client { get; set; }
        public int ID { get; set; }
        public IPEndPoint EndPoint { get; private set; }

        public event ClientMessageDelegate MessageReceived;
        public event StatusDelegate Disconnected;
        public event ErrorDelegate ConnectionError;

        private Thread _receiveMessageThread;
        private Thread _sendMessageThread;
        private NetworkStream _socketStream;
        private ConcurrentQueue<Message> _sendMessageQueue = new ConcurrentQueue<Message>();

        //private ManualResetEvent _mreSend = new ManualResetEvent(false);
        //private ManualResetEvent _mreRead = new ManualResetEvent(false);

        public void StartReceiving()
        {
            if (Client != null)
            {
                Client.SendBufferSize = Constants.MESSAGE_BUFFER_SIZE;
                Client.ReceiveBufferSize = Constants.MESSAGE_BUFFER_SIZE;
                Client.NoDelay = false;
                IsConnected = true;
                EndPoint = (IPEndPoint)Client.Client.RemoteEndPoint;
            }
            else
                throw new NullReferenceException("TCPClient is not set");

            _socketStream = Client.GetStream();

            _receiveMessageThread = new Thread(new ThreadStart(RecieveMessageLoop));
            _receiveMessageThread.IsBackground = true;
            _receiveMessageThread.Start();

            _sendMessageThread = new Thread(new ThreadStart(SendMessageLoop));
            _sendMessageThread.IsBackground = true;
            _sendMessageThread.Start();
        }

        private void RecieveMessageLoop()
        {
            byte[] readBuffer = new byte[Constants.MESSAGE_BUFFER_SIZE];
            int bytesRead;

            try
            {
                while (IsConnected)
                {
                    bytesRead = _socketStream.Read(readBuffer, 0, readBuffer.Length);

                    if (bytesRead == 0) // Endpoint disconnected, so end the message loop thread
                    {
                        IsConnected = false;
                        break;
                    }

                    Message msg = MessageSerializer.Deserialize(readBuffer);
                    MessageReceived?.Invoke(msg.SourceID, msg);
                }

            }
            catch (Exception e)
            {
                if (!(e is ObjectDisposedException)) // If we get this exception, it means Close() was manually called on the socket, and the stream was disposed already
                    ConnectionError?.Invoke(ID, e);
            }
            finally
            {
                IsConnected = false;

                if (Client.Connected)
                    Client.Close();

                Disconnected?.Invoke(ID);
            }
        }

        private void SendMessageLoop()
        {
            while (IsConnected)
            {
                Message message;
                if (!_sendMessageQueue.TryDequeue(out message))
                {
                    Thread.Sleep(10);
                    continue;
                }

                SendMessage(message);
            }
        }
        
        public void AddMessage(Message message)
        {
            _sendMessageQueue.Enqueue(message);
        }

        private void SendMessage(Message message)
        {
            try
            {
                if (IsConnected)
                {
                    byte[] sendBuffer = MessageSerializer.Serialize(message);

                    if (_socketStream.CanWrite)
                        _socketStream.Write(sendBuffer, 0, sendBuffer.Length);
                }
            }
            catch (Exception e)
            {
                ConnectionError?.Invoke(ID, e);
            }
        }

        public void Connect(IPEndPoint address)
        {
            Client = new TcpClient();
            Client.Connect(address);

            if (Client.Connected)
                IsConnected = true;
        }

        public void Disconnect()
        {
            IsConnected = false;
            Client.Close();
        }
    }
}
