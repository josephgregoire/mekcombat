﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MekCombat.Network
{
    public class MessageFactory
    {

        public Dictionary<MessageType, Dictionary<string, int>> MessageCodes { get; private set; }

        public MessageFactory(Dictionary<MessageType, Dictionary<string, int>> codes)
        {
            MessageCodes = codes;
        }

        public Message CreateMessage(MessageType type, int id, string code)
        {
            return CreateMessage(type, id, code, null);
        }

        public Message CreateMessage(MessageType type, int id, string code, params object[] data)
        {
            int msgCode;

            if (MessageCodes.TryGetValue(type, out Dictionary<string, int> codes))
            {
                if (!codes.TryGetValue(code, out msgCode))
                    return null;
            }
            else
                return null;

            Message msg = new Message()
            {
                SourceID = id,
                Type = type,
                MessageCode = msgCode,
                Data = data
            };

            return msg;
        }
    }
}
