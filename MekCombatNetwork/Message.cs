﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MekCombat.Network
{
    [StructLayout(LayoutKind.Sequential, Pack = 1), Serializable]
    public class Message
    {
        public int SourceID { get; internal set; }
        public MessageType Type { get; internal set; }
        public int MessageCode { get; internal set; }
        public object[] Data { get; internal set; }

        public Message() { }

        public void SetData(params object[] data)
        {
            this.Data = data;
        }
    }
}
